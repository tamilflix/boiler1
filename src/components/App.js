import React from 'react';
import '../styles/styles.scss'

const App = () => {

  const env = process.env.NODE_ENV;

  return (
    <div>
      <h1 className='some-class'>sample header</h1>
      <p>{`This is in ${env} environment`}</p>
    </div>
  );
};

export default App;
