import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/App";
import './styles/styles.scss'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

/* TODO: css/scss files need to exist in their respective components.
  Currently unable due to parsing errors. Need to fix.
 */