import App from "../../components/App";
import React from "react";
import {shallow} from "enzyme";

it('should get a matching snapshot',  () => {
  const wrapper = shallow(<App/>)
  expect(wrapper.find('h1').text()).toBe('sample header')
  expect(wrapper).toMatchSnapshot()
});