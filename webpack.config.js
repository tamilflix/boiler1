const path = require("path")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpack = require("webpack")

module.exports = () => {

  const isProduction = process.env.NODE_ENV === "production";
  console.log(`isProduction: ${isProduction} ${process.env.NODE_ENV}`)

  return {
    output: {
      path: path.resolve(__dirname, "build"),
      filename: "bundle.js"
    },
    resolve: {
      extensions: [".ts", ".tsx", ".js", ".jsx"]
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: "babel-loader"
            }
          ]
        },
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader"
            }
          ]
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: "file-loader"
            }
          ]
        },
        {
          test: /\.s[ac]ss$/i,
          use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
        }
      ]
    },

    plugins: [
      new HtmlWebpackPlugin({
        favicon: "./src/favicon.png",
        filename: "index.html",
        template: "./src/index.html"
      }),
      new MiniCssExtractPlugin(),
      new webpack.DefinePlugin({
        "process.env": {
          "NODE_ENV": JSON.stringify(process.env.NODE_ENV)
        }
      })
    ],
    devtool: isProduction ? 'source-map' : 'eval-cheap-module-source-map',
    devServer: {
      historyApiFallback: true,
      port: 5000
    }
  }
}